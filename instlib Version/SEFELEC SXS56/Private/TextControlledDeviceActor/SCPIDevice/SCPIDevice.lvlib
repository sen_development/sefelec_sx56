﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="GetESE Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetESE Msg/GetESE Msg.lvclass"/>
		<Item Name="GetESR Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetESR Msg/GetESR Msg.lvclass"/>
		<Item Name="GetIDN Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/IDN Msg/GetIDN Msg.lvclass"/>
		<Item Name="GetLRN Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetLRN Msg/GetLRN Msg.lvclass"/>
		<Item Name="GetSRE Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetSRE Msg/GetSRE Msg.lvclass"/>
		<Item Name="GetSTB Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetSTB Msg/GetSTB Msg.lvclass"/>
		<Item Name="GetTST Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/GetTST Msg/GetTST Msg.lvclass"/>
		<Item Name="SendCommand Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SendCommand Msg/SendCommand Msg.lvclass"/>
		<Item Name="SetCLS Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetCLS Msg/SetCLS Msg.lvclass"/>
		<Item Name="SetESE Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetESE Msg/SetESE Msg.lvclass"/>
		<Item Name="SetGTL Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetGTL Msg/SetGTL Msg.lvclass"/>
		<Item Name="SetLLO Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetLLO Msg/SetLLO Msg.lvclass"/>
		<Item Name="SetREM Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetREM Msg/SetREM Msg.lvclass"/>
		<Item Name="SetRST Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetRST Msg/SetRST Msg.lvclass"/>
		<Item Name="SetSRE Msg.lvclass" Type="LVClass" URL="../SCPIDevice Messages/SetSRE Msg/SetSRE Msg.lvclass"/>
	</Item>
	<Item Name="SCPIDevice.lvclass" Type="LVClass" URL="../../TextControlledDevice/SCPIDevice/SCPIDevice/SCPIDevice.lvclass"/>
</Library>
